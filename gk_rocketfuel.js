(function($) {

Drupal.behaviors.gk_rocketfuel = {
  attach: function (context, settings) {
    if (typeof Drupal.settings.gk_rocketfuel !== 'undefined') {
      var settings = Drupal.settings.gk_rocketfuel;
      var actionIDs = [];

      if (typeof settings.universalActionID != 'undefined') {
        actionIDs.push(settings.universalActionID);
      }

      if (typeof settings.urlActionID != 'undefined') {
        actionIDs.push(settings.urlActionID);
      }

      var cachebust = (Math.random() + "").substr(2);
      var url = 'p.rfihub.com/ca.gif?rb=' + settings.campaignID + '&ca=';
      var protocol = "https:" == document.location.protocol ? 'https:' : 'http:';

      for (var i in actionIDs) {
        new Image().src = protocol + '//' + actionIDs[i] + url + actionIDs[i] + '&ra=' + cachebust;
      }
    }
  }
};

})(jQuery);