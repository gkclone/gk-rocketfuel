<?php

/**
 * A form for configuring Rocket Fuel.
 */
function gk_rocketfuel_admin_config_form() {
  $form['gk_rocketfuel_campaign_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Campaign ID',
    '#default_value' => variable_get('gk_rocketfuel_campaign_id', ''),
  );

  $form['gk_rocketfuel_universal_action_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Universal action ID',
    '#description' => 'Add tracking to all pages of the site.',
    '#default_value' => variable_get('gk_rocketfuel_universal_action_id', ''),
  );

  $form['gk_rocketfuel_url_action_ids'] = array(
    '#type' => 'textarea',
    '#title' => 'URL to action ID associations',
    '#description' => 'Add tracking to specific pages.',
    '#default_value' => variable_get('gk_rocketfuel_url_action_ids', ''),
  );

  return system_settings_form($form);
}
